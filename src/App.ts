import * as express from 'express';
import * as morgan from 'morgan';

class App {
    public express;

    constructor() {
        this.express = express();
        this.express.use(morgan('dev'));
        this.mountRoutes();
    }

    private mountRoutes (): void {
        const router = express.Router();
        router.get('/', (req, res) => {
            res.json({
                message: 'Hello World!'
            });
        });
        this.express.use('/', router);
    }
}

export default new App().express;