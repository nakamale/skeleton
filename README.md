# Skelton-Title

## Skeleton for Node.js application written in TypeScript

the fallowing npm-scripts are supported:

### Development

```sh
npm run dev
```

### Linting

```bash
npm run lint
```

### Testing

```bash
npm test
```

### Building

```bash
npm run build
```

### Start

```bash
npm start
```

## Setup Test-Server (autom. restart)

- start local server

```bash
ngrok http 8080
```

- start app

```bash
npm run dev
npm run start:watch
```